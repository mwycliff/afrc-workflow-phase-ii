

/*      TITLE:               script.js                     
**                           
**      AUTHOR:              Matthew Wycliff               
**                           
**      DATE:                12/20/2019                   
**                           
**      LAST UPDATE:         12/20/2019                   
**                           
**      PURPOSE:             The purpose of this script is to
**                           pipe data from the Robotic Operating System
**                           To a web browser.
**                           Changes in the data will instigate various
**                           animations of the elements on screen.
**                           During normal use, data changes will 
**                           originate from commands send through
**                           PAX undergirded by Pride View.
**                           
**      ASSOCIATED FILES:    tf.html, see.css, airvolt.cpp 
**                           
**      CLONE COMMAND:       git clone git@bitbucket.org:mwycliff/elproverwms.git
**                           
**      RUN COMMAND:         xdg-open tf.html
**                           
**                           --then promptly hit 'refresh'
**                             once the airvolt ROS node is running.
**                           
*/                                                        

var temp_flux = 0;
var humidity_flux = 0;
var flux_counter = 3;
var flash = 0;
var gordon = 0;
var blink_counter = 0;
var blink_counter_latch = false;
const CHANNEL_ID = 'S9zR90COZmbxn8iW';
const drone = new ScaleDrone(CHANNEL_ID);

drone.on('close', event => console.log('Connection was closed', event));
drone.on('error', error => console.error(error));

const room = drone.subscribe('humidity', {
  historyCount: 1
});

room.on('data', data => {
  updateHumidityDOM(data.humidity);
});

const DOM = {
  updateButton: document.querySelector('button'),
  temperatureLabel: document.querySelector('.temperature'),
  motor_torque_port: document.querySelector('.motor_torque_port'),
  motor_torque_starboard: document.querySelector('.motor_torque_starboard'),
  blade_pitch_angle_port: document.querySelector('.blade_pitch_angle_port'),
  blade_pitch_angle_starboard: document.querySelector('.blade_pitch_angle_starboard'),
  precharge_voltage_percent: document.querySelector('.precharge_voltage_percent'),
  hv_battery_output_state: document.querySelector('.hv_battery_output_state'),
  precharger_state: document.querySelector('.precharger_state'),
  humidityLabel: document.querySelector('.humidity'),
  body: document.body,
  propellerLeft: document.getElementById('propellerLeft'),
  propellerRight: document.getElementById('propellerRight'),
  mcbox: document.getElementById('mcbox'),
  prechargerTile: document.getElementById('prechargerTile'),
  hvBatteryTile: document.getElementById('hvBatteryTile'),
};
  DOM.mcbox.classList.toggle("mcbox");

var humidity_structure = {
	name : 'humidity',
	color : 'white',
	value : DOM.humidityLabel,
	parity_value : 0,
	text_content : '',
	counter : 0,
	latch : false,
	driver_of : '',
}

var temperature_structure = {
	name : 'temperature',
	color : 'white',
	value : DOM.temperatureLabel,
	parity_value : 0,
	text_content : '',
	counter : 0,
	latch : false,
	driver_of : '',
}

var motor_torque_port_structure = {
	name : 'motor_torque_port',
	color : 'white',
	value : DOM.motor_torque_port,
	parity_value : 0,
	text_content : '',
	counter : 0,
	latch : false,
	driver_of : 'propellerLeft',
}

var motor_torque_starboard_structure = {
	name : 'motor_torque_starboard',
	color : 'white',
	value : DOM.motor_torque_starboard,
	parity_value : 0,
	text_content : '',
	counter : 0,
	latch : false,
	driver_of : 'propellerRight',
}

var blade_pitch_angle_port_structure = {
	name : 'blade_pitch_angle_port',
	color : 'white',
	value : DOM.blade_pitch_angle_port,
	parity_value : 0,
	text_content : '',
	counter : 0,
	latch : false,
}

var blade_pitch_angle_starboard_structure = {
	name : 'blade_pitch_angle_starboard',
	color : 'white',
	value : DOM.blade_pitch_angle_starboard,
	parity_value : 0,
	text_content : '',
	counter : 0,
	latch : false,
}

var precharge_voltage_percent_structure = {
	name : 'precharge_voltage_percent',
	color : 'white',
	value : DOM.precharge_voltage_percent,
	parity_value : 0,
	text_content : '',
	counter : 0,
	latch : false,
	driver_of : '',
}

var hv_battery_output_state_structure = {
	name : 'hv_battery_output_state',
	color : 'white',
	value : DOM.hv_battery_output_state,
	parity_value : 0,
	text_content : '',
	counter : 0,
	latch : false,
	driver_of : 'hvBatteryTile',
}

var precharger_state_structure = {
	name : 'precharger_state',
	color : 'white',
	value : DOM.precharger_state,
	parity_value : 0,
	text_content : '',
	counter : 0,
	counter_2 : 0,
	latch : false,
	driver_of : 'prechargerTile',
}

//======================   INITIALIZATION   ===========================

propellerAnimation(motor_torque_port_structure);
propellerAnimation(motor_torque_starboard_structure);
hvBatteryTileAnimation(hv_battery_output_state_structure);
prechargerTileAnimation(precharger_state_structure);

//======================   DATA STREAM   ==============================

  motor_torque_port_listener.subscribe(function(message) {

    flux_counter++;
    humidity_flux = Math.sin(flux_counter/2000)*10+10+65;
    temp_flux = Math.sin(flux_counter/900)*10+71;
    console.log("\n\n Temp =    " + temp_flux + "   Humidity =    : " + humidity_flux + "   " );
    sendHumidityMessage();
    temperature_structure.value = temp_flux;
    updateTemperatureDOM(temperature_structure.value);

    let r0 = JSON.stringify(message.data);
    motor_torque_port_structure.value = parseFloat(r0);

    updateTelemDOMs(motor_torque_port_structure,
     motor_torque_starboard_structure,
      blade_pitch_angle_port_structure,
       blade_pitch_angle_starboard_structure,
        precharge_voltage_percent_structure,
         hv_battery_output_state_structure,
          precharger_state_structure);
    
    propellerAnimation(motor_torque_port_structure);
    propellerAnimation(motor_torque_starboard_structure);
	hvBatteryTileAnimation(hv_battery_output_state_structure);
	prechargerTileAnimation(precharger_state_structure);
  });

  motor_torque_starboard_listener.subscribe(function(message) {
    let r0 = JSON.stringify(message.data);
    motor_torque_starboard_structure.value = parseFloat(r0);
  });

  blade_pitch_angle_port_listener.subscribe(function(message) {
    let r0 = JSON.stringify(message.data);
    blade_pitch_angle_port_structure.value = parseFloat(r0);
  });

  blade_pitch_angle_starboard_listener.subscribe(function(message) {
    let r0 = JSON.stringify(message.data);
    blade_pitch_angle_starboard_structure.value = parseFloat(r0);
  });

  precharge_voltage_percent_listener.subscribe(function(message) {
    let r0 = JSON.stringify(message.data);
    precharge_voltage_percent_structure.value = parseFloat(r0);
  });

  hv_battery_output_state_listener.subscribe(function(message) {
    r0 = JSON.stringify(message.data*1);
    hv_battery_output_state_structure.value = parseFloat(r0);
  });

  precharger_state_listener.subscribe(function(message) {
    r0 = JSON.stringify(message.data*1);
    precharger_state_structure.value = parseFloat(r0);
  });

function hvBatteryTileAnimation(tes) {
	if(tes.value === tes.parity_value) {}
	else {
		tes.parity_value = tes.value;
		if (tes.value === 1) {
			if(document.getElementById(`${tes.driver_of}`).classList.contains("on_state") === true) {} 
				else {
    				document.getElementById(`${tes.driver_of}`).className = ''
					document.getElementById(`${tes.driver_of}`).classList.toggle("on_state");
				}
		}
		else {
			if(document.getElementById(`${tes.driver_of}`).classList.contains("off_state") === true) {} 
			else {
				document.getElementById(`${tes.driver_of}`).className = ''
				document.getElementById(`${tes.driver_of}`).className = 'off_state'
			}
		}    
 	}
}

function prechargerTileAnimation(tes) {
	if(tes.value === tes.parity_value) {}
	else {
		tes.parity_value = tes.value;
		if (tes.value === 1) {
			tes.counter_2 = tes.counter_2 + 1;
			if ((tes.counter_2 % 2 === 0) && (tes.counter_2 % 3 === 0)) {
				if(document.getElementById(`${tes.driver_of}`).classList.contains("on_state_1") === true) {} 
				else {
					document.getElementById(`${tes.driver_of}`).className = ''
					document.getElementById(`${tes.driver_of}`).classList.toggle("on_state_1");
				}

			} else 	if ((tes.counter_2 % 2 === 1) && (tes.counter_2 % 3 === 0)) {
				if(document.getElementById(`${tes.driver_of}`).classList.contains("on_state_2") === true) {} 
				else {
					document.getElementById(`${tes.driver_of}`).className = ''
					document.getElementById(`${tes.driver_of}`).classList.toggle("on_state_2");
				}
			}
			else {}
			tes.parity_value = !tes.value;
		}
		else {
			if(document.getElementById(`${tes.driver_of}`).classList.contains("off_state") === true) {} 
			else {
				document.getElementById(`${tes.driver_of}`).className = ''
				document.getElementById(`${tes.driver_of}`).className = 'off_state'
			}
		}
 	}
}

/*     .This function takes in the telemetry_structure for the two propellers                
**     .And triggers an animation for the corresponding propeller.                 
**     tes --> Telemetry Element Structure
*/ 
function propellerAnimation(tes) {
    	if(tes.value === tes.parity_value) {}
    	else {
    		tes.parity_value = tes.value;
    		var speed = Math.round(tes.value / 10);
    		if (speed > 8) {
				if(document.getElementById(`${tes.driver_of}`).classList.contains("su1") === true) {} 
					else {
	    				document.getElementById(`${tes.driver_of}`).className = ''
						document.getElementById(`${tes.driver_of}`).classList.toggle("su1");
						DOM.mcbox.className = ''
						DOM.mcbox.classList.toggle("fly");
					}
    		}
    		else if (speed > 4) {
				if(document.getElementById(`${tes.driver_of}`).classList.contains("s4") === true) {} 
					else {
	    				document.getElementById(`${tes.driver_of}`).className = ''
						document.getElementById(`${tes.driver_of}`).classList.toggle("s4");
					}
    		}
    		else {
				if(document.getElementById(`${tes.driver_of}`).classList.contains("sd1") === true) {} 
					else {
	    				document.getElementById(`${tes.driver_of}`).className = ''
						document.getElementById(`${tes.driver_of}`).classList.toggle("sd1");
						DOM.mcbox.className = ''
						DOM.mcbox.classList.toggle("off_state");
				}
    		}
    	}
}

function updateTelemDOMs(motor_torque_port_structure,
     motor_torque_starboard_structure,
      blade_pitch_angle_port_structure,
       blade_pitch_angle_starboard_structure,
        precharge_voltage_percent_structure,
         hv_battery_output_state_structure,
          precharger_state_structure) {
    motor_torque_port_structure = fontColorController(motor_torque_port_structure); 
	updateDomElement(motor_torque_port_structure); // manipulates the dom element by
                    	                           // referencing the the document.querySelector
    motor_torque_starboard_structure = fontColorController(motor_torque_starboard_structure); 
	updateDomElement(motor_torque_starboard_structure);
    blade_pitch_angle_port_structure = fontColorController(blade_pitch_angle_port_structure); 
	updateDomElement(blade_pitch_angle_port_structure);
    blade_pitch_angle_starboard_structure = fontColorController(blade_pitch_angle_starboard_structure); 
	updateDomElement(blade_pitch_angle_starboard_structure);
    precharge_voltage_percent_structure = fontColorController(precharge_voltage_percent_structure); 
	updateDomElement(precharge_voltage_percent_structure);
    hv_battery_output_state_structure = fontColorController(hv_battery_output_state_structure); 
	updateDomElement(hv_battery_output_state_structure);
    precharger_state_structure = fontColorController(precharger_state_structure); 
	updateDomElement(precharger_state_structure);
}

/*          .This function changes each telemetry value's font colors
*           .fater their numerical or boolean values change.                                                   
*/                                                              
function fontColorController(telemetry_element_structure) {
    telemetry_element_structure.latch = (telemetry_element_structure.text_content === `${telemetry_element_structure.name}:  ${telemetry_element_structure.value}`) ? false : true;
    telemetry_element_structure.counter = (telemetry_element_structure.counter % 3 === 0) ? 0 : telemetry_element_structure.counter;
    if ((telemetry_element_structure.counter !== 0)||(telemetry_element_structure.latch !== false)) {
    
        telemetry_element_structure.counter = telemetry_element_structure.counter + 1;
        telemetry_element_structure.color = (telemetry_element_structure.counter % 2 === 0) ? 'cyan' : 'yellow';
    }
    else {
        telemetry_element_structure.latch = false;
        switch(typeof telemetry_element_structure.value) {
        case 'number':
            telemetry_element_structure.color = ((telemetry_element_structure.value === 0) ? 'palegreen' : 'orange');
            break;
        case 'boolean':
            telemetry_element_structure.color = ((telemetry_element_structure.value === true) ? 'palegreen' : 'orange');
            break;
        default:
            telemetry_element_structure.color = ((telemetry_element_structure.value === true) ? 'palegreen' : 'orange');
            break;
        }
    }
    telemetry_element_structure.text_content = `${telemetry_element_structure.name}:  ${telemetry_element_structure.value}`;
    return telemetry_element_structure;
}

//          .This function changes each DOM after the corresponding telemetry element structure is updated
function updateDomElement(telemetry_element_structure) {
	document.querySelector(`.${telemetry_element_structure.name}`).style.color = telemetry_element_structure.color;
	document.querySelector(`.${telemetry_element_structure.name}`).textContent = telemetry_element_structure.text_content;
}


function sendHumidityMessage() {
  drone.publish({
    room: 'humidity',
    message: {
  	  humidity: Math.round(humidity_flux)
    },
  });
}

function updateTemperatureDOM(temperature) {
  DOM.temperatureLabel.textContent = `Temp: ${Math.round(temperature)}°F`;
}

function updateHumidityDOM(humidity) {
  DOM.humidityLabel.textContent = `Humidity: ${humidity} %`;
}
