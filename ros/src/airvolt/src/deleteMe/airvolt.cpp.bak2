/** 
 ** Name:		airvolt.cpp 
 ** Author:		Matthew Wycliff 
 ** Date Created:	December 2, 2019		
 ** Last Update:	January 5, 2020		
 ** Description:
 ** 
 ** ROS node for demonstrating the AIRVOLT2 system
 ** representation (sysrep). Developed to interface with 'jrosbridge' on
 ** the PRIDE end (embedded in PRIDE's PAX java code) using 'rosbridge' as
 ** the go-between. TODO.
 ** 
**/

/**
 * Put copyright notice here
 */

#include <ros/ros.h>
#include <airvolt/Open.h>
#include <airvolt/Close.h>
#include <airvolt/ContentsSet.h>
#include <airvolt/DialSet.h>
#include <airvolt/FloatSet.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <std_msgs/Header.h>
#include <iostream>
#include <std_srvs/SetBool.h>

// SSYE state
bool m_switch = true;
int  m_dial = 5;
int  m_contents = 16;
double  m_bladeAnglePort = 0.0;
double  m_bladeAngleStarboard = -0.002;
double  m_torquePort = 0;
double  m_torqueStarboard = 0;
double  m_voltagePercent = 0.04;

//bool hv_battery_output_state = true;
bool hv_battery_output_state = false;
bool precharger_state = false;
bool initiatePrecharge = false;

std::string m_name = "Input";
double m_mult = 45000;
const int DIAL_MIN = 0;
const int DIAL_MAX = 10;
const int BLADE_ANGLE_MIN = -45;
const int BLADE_ANGLE_MAX = 45;
const int TORQUE_MIN = 0;
const int TORQUE_MAX = 60000;

// message test; want to show changing value at run
std::string m_frame1 = "world";
std::string m_frame2 = "map";

// publishers
ros::Publisher motor_torque_port_pub;
ros::Publisher motor_torque_starboard_pub;
ros::Publisher blade_pitch_angle_port_pub;
ros::Publisher blade_pitch_angle_starboard_pub;
ros::Publisher precharge_voltage_percent_pub;
ros::Publisher hv_battery_output_state_pub;
ros::Publisher precharger_state_pub;

ros::Publisher switchPub;
ros::Publisher dialPub;
ros::Publisher contentsPub;
ros::Publisher namePub;
ros::Publisher pressurePub;
ros::Publisher headerPub;

// **************************************************
// **** commands (using ROS service calls)
bool switchOpen(airvolt::Open::Request  &req,
                airvolt::Open::Response &rsp) {
	rsp.was = m_switch;
	m_switch = true;
	ROS_INFO("Switch: was [%s], set to [%s]",
	         (rsp.was ? "true" : "false"),
	         (m_switch ? "true" : "false"));
	return true;
}

bool switchClose(airvolt::Close::Request  &req,
                 airvolt::Close::Response &rsp) {
	rsp.was = m_switch;
	m_switch = false;
	ROS_INFO("Switch: was [%s], set to [%s]",
	         (rsp.was ? "true" : "false"),
	         (m_switch ? "true" : "false"));
	return true;
}

bool contentsSet(airvolt::ContentsSet::Request  &req,
                 airvolt::ContentsSet::Response &rsp) {
	rsp.was = m_contents;
	if (req.set != 0 && req.set != 1) {
		ROS_ERROR("Contents: invalid contents type [%d]", req.set);
		return false;
	}
	m_contents = req.set;
	ROS_INFO("Contents: was [%d], set to [%d]", rsp.was, m_contents);
	return true;
}

bool dialSet(airvolt::DialSet::Request  &req,
             airvolt::DialSet::Response &rsp) {
	rsp.was = m_dial;
	m_dial = (req.set < DIAL_MIN ? DIAL_MIN :
	                             (req.set > DIAL_MAX ? DIAL_MAX : req.set));
	ROS_INFO("Dial: was [%d], set to [%d]", rsp.was, m_dial);
	return true;
}

// **************************************************

bool bladeAnglePortSet(airvolt::FloatSet::Request  &req,
             		   airvolt::FloatSet::Response &rsp) {
	rsp.was = m_bladeAnglePort;
	m_bladeAnglePort = (req.set < BLADE_ANGLE_MIN ? BLADE_ANGLE_MIN :
	                   (req.set > BLADE_ANGLE_MAX ? BLADE_ANGLE_MAX : req.set));
	ROS_INFO("Blade Angle Port: was [%d], set to [%d]", rsp.was, m_bladeAnglePort);
	return true;
}

bool bladeAngleStarboardSet(airvolt::FloatSet::Request  &req,
             		   		airvolt::FloatSet::Response &rsp) {
	rsp.was = m_bladeAngleStarboard;
	m_bladeAngleStarboard = (req.set < BLADE_ANGLE_MIN ? BLADE_ANGLE_MIN :
	                   		(req.set > BLADE_ANGLE_MAX ? BLADE_ANGLE_MAX : req.set));
	ROS_INFO("Blade Angle Starboard: was [%d], set to [%d]", rsp.was, m_bladeAngleStarboard);
	return true;
}

bool motorTorquePortSet(airvolt::FloatSet::Request  &req,
             		   	airvolt::FloatSet::Response &rsp) {
	rsp.was = m_torquePort;
	m_torquePort = (req.set < TORQUE_MIN ? TORQUE_MIN :
	               (req.set > TORQUE_MAX ? TORQUE_MAX : req.set));
	ROS_INFO("Port side motor torque: was [%d], set to [%d]", rsp.was, m_torquePort);
	return true;
}

bool motorTorqueStarboardSet(airvolt::FloatSet::Request  &req,
             		   		 airvolt::FloatSet::Response &rsp) {
	rsp.was = m_torqueStarboard;
	m_torqueStarboard = (req.set < TORQUE_MIN ? TORQUE_MIN :
	               		(req.set > TORQUE_MAX ? TORQUE_MAX : req.set));
	ROS_INFO("Starboard side motor torque: was [%d], set to [%d]", rsp.was, m_torqueStarboard);
	return true;
}

bool hvBatteryOutputStateSet(std_srvs::SetBool::Request  &req,
             		   		 std_srvs::SetBool::Response &rsp) {
	rsp.success = true;
	rsp.message = "RECEIVED";
	int was = hv_battery_output_state;
	hv_battery_output_state = req.data;
	ROS_INFO("High voltage battery state: was [%d], set to [%d]", was, hv_battery_output_state);
	return true;
}

bool prechargeLogic(bool arg) {
	if (arg == true)
		m_voltagePercent = 95.5;
	else
		m_voltagePercent = 0;
}

bool prechargerStateSet(std_srvs::SetBool::Request  &req,
             		    std_srvs::SetBool::Response &rsp) {
	rsp.success = true;
	rsp.message = "";
	int was = precharger_state;
	//if(was==)
	//newConditional = (was == false && req.data == true ? a : b )
	precharger_state = req.data;
	//newConditional = (was == false && req.data == true ? a : b )
	initiatePrecharge = (was == false && req.data == true ? true : false );
	ROS_INFO("Pre-charger state: was [%d], set to [%d]", was, precharger_state);
	return true;
}

void spinPub(unsigned int cnt) {
	std_msgs::Bool boolMsg;
	boolMsg.data = m_switch;
	switchPub.publish(boolMsg);
	
	std_msgs::Int32 intMsg;
	intMsg.data = m_dial;
	dialPub.publish(intMsg);
	intMsg.data = m_contents;
	contentsPub.publish(intMsg);
	
	std_msgs::String strMsg;
	strMsg.data = m_name;
	namePub.publish(strMsg);
	
	std_msgs::Float64 dblMsg;
	dblMsg.data = (m_switch == false ? 0.0 : (m_dial * m_mult));
	pressurePub.publish(dblMsg);	
	
	dblMsg.data = m_bladeAnglePort;
	blade_pitch_angle_port_pub.publish(dblMsg);	
	
	dblMsg.data = m_bladeAngleStarboard;
	blade_pitch_angle_starboard_pub.publish(dblMsg);
	
	dblMsg.data = m_torquePort;
	motor_torque_port_pub.publish(dblMsg);
	
	dblMsg.data = m_torqueStarboard;
	motor_torque_starboard_pub.publish(dblMsg);
	
	prechargeLogic(initiatePrecharge);
	dblMsg.data = m_voltagePercent;
	precharge_voltage_percent_pub.publish(dblMsg);

	boolMsg.data = hv_battery_output_state;
	hv_battery_output_state_pub.publish(boolMsg);	

	boolMsg.data = precharger_state;
	precharger_state_pub.publish(boolMsg);	

	std_msgs::Header headerMsg;
	headerMsg.seq = cnt;
	headerMsg.stamp = ros::Time::now();
	headerMsg.frame_id = (cnt % 2 ? m_frame1 : m_frame2);
	headerPub.publish(headerMsg);
}

// **************************************************
// **** node definition/functionality
int main(int argc, char **argv) {
	// NOTE: set nodehandle's base namespace to private namespace; remap as
	// desired in launch file (e.g., '~'->'jros' or '/airvolt'->'/jros')
	ros::init(argc, argv, "airvolt", ros::init_options::AnonymousName);
	ros::NodeHandle nh("~");
	
	// state (telemetry) publishers	
	switchPub   = nh.advertise<std_msgs::Bool>("switch", 10);
	dialPub     = nh.advertise<std_msgs::Int32>("dial", 10);
	contentsPub = nh.advertise<std_msgs::Int32>("contents", 10);
	namePub     = nh.advertise<std_msgs::String>("name", 10);
	pressurePub = nh.advertise<std_msgs::Float64>("pressure", 10);
	headerPub   = nh.advertise<std_msgs::Header>("header", 10);
	
	motor_torque_port_pub			= nh.advertise<std_msgs::Float64>("motor_torque_port", 10);
	motor_torque_starboard_pub		= nh.advertise<std_msgs::Float64>("motor_torque_starboard", 10);
	blade_pitch_angle_port_pub		= nh.advertise<std_msgs::Float64>("blade_pitch_angle_port", 10);
	blade_pitch_angle_starboard_pub		= nh.advertise<std_msgs::Float64>("blade_pitch_angle_starboard", 10);
	precharge_voltage_percent_pub		= nh.advertise<std_msgs::Float64>("precharge_voltage_percent", 10);
	hv_battery_output_state_pub		= nh.advertise<std_msgs::Bool>("hv_battery_output_state", 10);
	precharger_state_pub			= nh.advertise<std_msgs::Bool>("precharger_state", 10);
	
	// command services
	ros::ServiceServer openSrv      = nh.advertiseService("open", switchOpen);
	ros::ServiceServer closeSrv     = nh.advertiseService("close", switchClose);
	ros::ServiceServer contentsSrv  = nh.advertiseService("contents_set", contentsSet);
	ros::ServiceServer dialSrv      = nh.advertiseService("dial_set", dialSet);
	
	ros::ServiceServer set_blade_pitch_angle_port_side = nh.advertiseService("set_blade_pitch_angle_port_side", bladeAnglePortSet);
	ros::ServiceServer set_blade_pitch_angle_starboard_side = nh.advertiseService("set_blade_pitch_angle_starboard_side", bladeAngleStarboardSet);
	ros::ServiceServer set_motor_torque_port_side = nh.advertiseService("set_motor_torque_port_side", motorTorquePortSet);
	ros::ServiceServer set_motor_torque_starboard_side = nh.advertiseService("set_motor_torque_starboard_side", motorTorqueStarboardSet);
	ros::ServiceServer hv_battery_output_en = nh.advertiseService("hv_battery_output_en", hvBatteryOutputStateSet);
	ros::ServiceServer precharge_on_cmd = nh.advertiseService("precharge_on_cmd", prechargerStateSet);
	// initialize ros::time
	ros::Time begin = ros::Time::now();	
	double secs =ros::Time::now().toSec();
	ros::Duration d(0.5);
	//secs = d.toSec();
	
	ros::Rate spinRate(10); // Hz
	unsigned int count = 0; // unsigned never overflows, but wraps
	while (ros::ok()) {
		spinPub(count++);
		ros::spinOnce();
		spinRate.sleep();
		//time-related-stuff
		//secs = d.toSec();
		secs =ros::Time::now().toSec();
		ROS_INFO("\n\n     time duration d.toSec():    [%f]   \n", secs);
	}
	return 0;
}

