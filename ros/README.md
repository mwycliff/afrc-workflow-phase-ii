** sudo apt install python-clint python-rosinstall python-catkin-tools

** If on melodic, `git chekout melodic-devel` at the top level.

** run `./install.py`

** `rosdep install --from-path src/ --ignore-src` 

**Build by:**
```
catkin config -DCMAKE_BUILD_TYPE=Release --extend /opt/ros/kinetic
catkin build
```

**Launch by:**
```
source devel/setup.bash
roslaunch craftsman_robots tracbot.launch
```

** VERSION_INFO.txt

* As you add branches to repos, you can make sections in the VERSION_INFO.txt so that when you run `./install.py -v TAGNAME`, it will pull those repos/branches instead of the default ones in the LATEST tag.
