#!/bin/bash
#declare slpA=1
declare slpA=2
#export PRIDEDIR=${HOME}/traclabs/pride
export PRIDEDIR=${AFRC_WORKFLOW_PHASE_II_DIR}/pride
export BROWSERACTION=${AFRC_WORKFLOW_PHASE_II_DIR}/browser_component/web_browser_ui/src
export ROSACTION=${AFRC_WORKFLOW_PHASE_II_DIR}/ros
#export PRIDEURL="https://localhost:8000/dashboard?settings
#=%7B%22available%22%3A%7B%22searchQuery%22%3A%5B%7B%22key%
#22%3A%22search%22%2C%22value%22%3A%22airv%22%2C%22type%22%
#3A%22freeform%22%7D%5D%7D%2C%22initiated%22%3A%7B%22onlyMine%
#22%3Afalse%7D%7D"
export PRIDEURL="https://localhost:8000/"



tab="--tab"

two=""
twofish="bash -c 'cd ${PRIDEDIR}/view/code;npm start';bash"

four=""
fourfish="bash -c 'cd ${BROWSERACTION};xdg-open tf.html';bash"

five=""
fivefish="bash -c 'cd ${ROSACTION};source devel/setup.bash;roslaunch airvolt airvolt.launch';bash"

six=""
sixfish="bash -c 'firefox --new-tab ${PRIDEURL}';bash"

seven=""
sevenfish="bash -c 'cd ${PRIDEDIR}/automate/code;./run.sh --configFile ./resources/PaxPropertiesAirvolt2.yaml';bash"

sleep $slpA
for i in 1; do
      two+=($tab -e "$twofish")         
done
gnome-terminal "${two[@]}"
xdotool windowminimize $(xdotool getactivewindow)

sleep $slpA
for i in 1; do
      four+=($tab -e "$fourfish")         
done
gnome-terminal "${four[@]}"

sleep $slpA
for i in 1; do
      five+=($tab -e "$fivefish")         
done
gnome-terminal "${five[@]}"
xdotool windowminimize $(xdotool getactivewindow)

sleep $slpA
for i in 1; do
      six+=($tab -e "$sixfish")         
done
gnome-terminal "${six[@]}"

sleep $slpA
for i in 1; do
      seven+=($tab -e "$sevenfish")         
done
gnome-terminal "${seven[@]}"
xdotool windowminimize $(xdotool getactivewindow)

exit 0
