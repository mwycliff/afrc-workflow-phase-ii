#!/bin/bash
# generic install/update script for PRIDE/ROS/etc projects
# see ./startup/build*.sh for build scripts
source ./set_project.sh
PSTAT=$?
if [ "$PROJECT_DIR" = "" ]; then
  echo "Do you need to source .bashrc?"
  exit 1
elif [ "$PSTAT" != "0" ] || [ "$PROJECT_NAME_VAR" = "" ]; then
  echo "Project ${PROJECT} environment failed"
  exit 1
fi
echo "PROJECT: PROJECT_DIR environment variable directory set to:"
echo "${PROJECT_NAME_VAR}: ${PROJECT_DIR}"
CRAFTSMAN_CLONE_CMD='git@bitbucket.org:traclabs/pharaoh.git' # assumes SSH keys
CRAFTSMAN_BRANCH='kinetic-devel'
CRAFTSMAN_VERSION='LATEST'
#PRIDE_BRANCH='release/6.1.0'
#PRIDE_BRANCH='feature/pharaoh-devel'
PRIDE_BRANCH='feature/afrc_workflow-phase-II-0.2.2'
#PRIDE_BRANCH='feature/afrc_workflow-phase-II'
PRIDE_PRODUCT=''
PRIDE_PRODUCT_BRANCH=''
PRIDE_PROCEDURE_SVN='https://svn.traclabs.com/svn/pride/trunk/procedures/pharaoh_phase2'
#PRIDE_OWLSYSREP_SVN='https://svn.traclabs.com/svn/ontology/trunk/docs/ontologies'
PRIDE_YAMLSYSREP_SVN='https://svn.traclabs.com/svn/ontology/trunk/docs/yaml_sysreps/pharaoh'

DEFAULT_BRANCH='master'

# TODO: decide whether to make PRIDE single-branch checkout
# TODO: may need to add RuleEngine, UnReal, etc...

#### ROS workspace
# note: cd to ros for another install script
# for more information about installing ROS CRAFTSMAN see:
# https://traclabs.atlassian.net/wiki/spaces/CRAFTSMAN/pages/69341818/Installation

#### PRIDE

cd ${PROJECT_DIR}
if [ ! -d "./pride" ]; then
  # see 'pride/view/code/README.md' for installation notes
  echo "Getting PRIDE repository..."
  if [ "$PRIDE_BRANCH" = "" ]; then
    git clone -b ${DEFAULT_BRANCH} git@bitbucket.org:traclabs/pride.git
  else
    git clone -b ${PRIDE_BRANCH} git@bitbucket.org:traclabs/pride.git
  fi
  PSTAT=$?
  if [ "$PSTAT" != "0" ]; then
    echo "PRIDE repo failed!"
    exit 1
  fi
  # assume that if pride is there, database is initialized


#echo "Updating PRIDE repository..."
#  cd pride
#  if [ "$PRIDE_BRANCH" != "" ]; then
#    git checkout $PRIDE_BRANCH
#  fi
#  git pull
#fi
#cd ${PROJECT_DIR}/pride
#if [ -d "./view/code" ]; then
#  cd ./view/code
#  if [ ! -d "./services" ]; then
#    echo "Creating services directory..."
#    mkdir services
#  fi
#  

# TODO: always rebuild database?
  #NPM_CHECK=`which npm`
  #if [ "$NPM_CHECK" != "" ]; then
  #  echo "(Re-)Initializing database (may need mysql root password)..."
  #  npm run init
  #else
  #  echo "Cannot find npm to initialize PRIDE database!"
  #  exit 1
  #fi
fi

#sudo apt install python-rosdep
#FOR MELODIC ONLY
#rosdep install --from-path ./ros/src/airvolt/ --ignore-src

#FOR KINETIC-KAME
#catkin config -DCMAKE_BUILD_TYPE=Release --extend /opt/ros/kinetic

#sudo rosdep init
#rosdep update
#sudo apt install python-catkin-pkg






#cd ros
#
#if [ ! -d "./.catkin_tools" ]; then
#  catkin init
#  PSTAT=$?
#  if [ "$PSTAT" != "0" ] || [ ! -d "./.catkin_tools" ]; then
#    echo "catkin init failed!"
#    exit 1
#  fi
#fi
#if [ ! -d "./.catkin_tools/profiles" ]; then
#  catkin config -DCMAKE_BUILD_TYPE=Release --extend /opt/ros/kinetic
#  PSTAT=$?
#  if [ "$PSTAT" != "0" ] || [ ! -d "./.catkin_tools/profiles" ]; then
#    echo "catkin config failed!"
#    exit 1
#  fi
#fi
#


cd ${PROJECT_DIR}
mkdir ./pride/view/code/prl
cp ./miscellaneous/prl/* ./pride/view/code/prl
printf "\ncd \$AFRC_WORKFLOW_PHASE_II_DIR\n" >> ~/.bashrc

