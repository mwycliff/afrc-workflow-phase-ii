# Title:		AFRC-WORKFLOW-PHASE-II
# Author:		Matthew Wycliff
# Date:			January 9, 2020
# Last Update:	March 12, 2020

Repository for the AFRC phase-II project.

## Sections
* [Pre-requisite software](#markdown-header-pre-requisite-software)
* [Cloning](#markdown-header-cloning)
* [Quickstart](#markdown-header-installation)

## Pre-requisite software

* [PRIDE bitbucket README](https://bitbucket.org/traclabs/pride/src/develop/view/code/README.md)
* [My-SQL]
* [ROS]

## Cloning:
Clone this repository with the command (assumes you have SSH keys set up):
```bash
git clone git@bitbucket.org:traclabs/afrc-workflow-phase-II.git
```
For the remainder of this README, the _top-level directory_ refers to the root of the cloned repository; the above command will result in the top-level directory `afrc-workflow-phase-II`.

## Quickstart

1.  After cloning, switch to the top-level directory and run `./install.sh`.

2.  source ~/.bashrc       and then     cd $AFRC_WORKFLOW_PHASE_II_DIR

3.	cd pride/

4.	git pull origin release/6.1.0

5.	cd view/code

6.  ** open the README.md file in your favourite text editor, or go
		% less README.md and follow the instructions to install and 
		configure PRIDE on an Ubuntu Linux machine.

		Make sure to execute each step -- including the step where you
		pull down the prl/ directory and all of its contents.

7.	Once you are through installing, configuring, and testing PRIDE view,
	you may cd ../../automate/code/

8.  ant build

9.	cd ../../../../startup

10. ./buildAll

11.  source ~/.bashrc

12.  cd ../ros/              and then     source /opt/ros/kinetic/setup.bash

13.  catkin build            and then     cd ../

14. ./showtime

15.  Feng Shui the windows -- Align PRIDE-VIEW on the right, and align the Airvolt UI on the left.

16.  Refresh both windows until you see data on both browsers.

17.  Sign into PRIDE View ( un: test  pw: test )

18.  Search for the Airvolt 5058 Prl -- enter the search key "5058".

19. Run the PRL  -- click 'play', 'automate', and 'start'.

20. When you are through, you may run the
	closecurtains.sh bash script from the terminal.



