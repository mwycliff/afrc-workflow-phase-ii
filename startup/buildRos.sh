#!/bin/bash
# generic build script for ROS project part
if [ "$1" = "" ]; then
  source ../set_project.sh --no-bashrc-mods # sets PROJECT_DIR
  PSTAT=$?
  if [ "$PSTAT" != "0" ]; then
    echo "Environment failed, refusing to build"
    echo "Do you need to run install.sh or source ~/.bashrc?"
    read -p "Press [Enter] key to finish..."
    exit 1
  fi
else
  PROJECT_DIR=$1
fi
if [ "$PROJECT_DIR" = "" ] || [ ! -d "$PROJECT_DIR" ]; then
	# confirm we have an extant project directory (i.e., if given a parameter);
	# if set, assume the directory is correct, 'cuz we can only do so much...
  echo "${PROJECT_DIR} failed, refusing to build"
  echo "Do you need to source ~/.bashrc?"
  read -p "Press [Enter] key to finish..."
  exit 1
elif [ ! -d "${PROJECT_DIR}/ros/.catkin_tools/profiles" ]; then
  echo "*** catkin installation failure, refusing to build"
  echo "Do you need to 'cd ..; ./install.sh'?"
  read -p "Press [Enter] key to finish..."
  exit 1
fi

# build ROS stuff
echo
echo "************************"
echo "Building ROS packages..."
CATKIN=`which catkin`
if [ "$CATKIN" = "" ]; then
  # have to do initial ROS setup
  source /opt/ros/kinetic/setup.bash
  PSTAT=$?
  if [ "$PSTAT" != "0" ]; then
    echo "ROS environment setup failed!"
    read -p "Press [Enter] key to finish..."
    exit 1
  fi
fi
cd ${PROJECT_DIR}/ros
catkin build

# check ROS_PACKAGE_PATH; if current project not there, offer bashrc alias
if [ -d "./devel" ]; then
  VAR_CHECK=`echo $ROS_PACKAGE_PATH | grep ${PROJECT_DIR}`
  if [ "$VAR_CHECK" = "" ]; then
    echo
    echo "*** Missing $PROJECT paths in ROS_PACKAGE_PATH"
    # assume '$PROJECT/ros/devel' in .bashrc is the alias
    BASHRC_CHECK=`cat ~/.bashrc | grep ${PROJECT_DIR}/ros/devel`
    if [ "$BASHRC_CHECK" = "" ]; then
      read -e -r -p "*** Write 'alias devros_${PROJECT}' to .bashrc (yes/no)? "
      if [ "$REPLY" = "yes" ] || [ "$REPLY" = "YES" ]; then
        echo "*** NOTE: Adding ROS setup alias to .bashrc..."
        echo "" >> ~/.bashrc
        echo "# auto-written ROS setup for ${PROJECT} project" >> ~/.bashrc
        echo "alias devros_${PROJECT}='source ${PROJECT_DIR}/ros/devel/setup.bash'" >> ~/.bashrc
        echo "*** NOTE: 'source ~/.bashrc; devros_${PROJECT}' REQUIRED"
      else
        echo "*** NOT modifying .bashrc..."
      fi
    else
      echo "*** NOTE: 'source ~/.bashrc; devros_${PROJECT}' to update ROS_PACKAGE_PATH"
      read -p "Press [Enter] key to finish..."
      # exit 1
    fi
  fi
fi
source ./devel/setup.bash
rospack profile

