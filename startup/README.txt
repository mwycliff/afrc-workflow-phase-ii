##.  ADDITIONAL INSTRUCTIONS FOR STARTUP

If you have PRIDE already installed somewhere on your machine
you may proceed by running the buildAll.sh bash script.

$ ./buildAll.sh

If PRIDE is not installed anywhere on your machine, copy and paste
the line below into your terminal and hit ENTER.  Once those
processes are complete, you may proceed by running buildAll.sh.

pushd .

source ~/.bashrc

popd

cd ../

sudo rosdep install --from-path ./ros/src/airvolt/ --ignore-src --rosdistro kinetic

cd ./pride/view/code

sudo apt-get install python-software-properties build-essential g++ mysql-server mysql-client ant subversion openjdk-8-jdk-headless

curl --silent --location https://deb.nodesource.com/setup_8.x | sudo bash -

sudo apt-get install -y nodejs

npm run mysql-reset

sudo npm run mysql-init




