#!/bin/bash
# generic build script for PRIDE/ROS/etc projects
source ../set_project.sh --no-bashrc-mods # sets PROJECT_DIR
PSTAT=$?
if [ "$PSTAT" != "0" ] || [ "$PROJECT_NAME_VAR" = "" ]; then
  echo "Project ${PROJECT} environment failed, refusing build"
  echo "Do you need to run install.sh or source ~/.bashrc?"
  exit 1
fi

# general structure of child/sub repository building:
## build <sub_repo>
#echo
#echo "******************"
#echo "Building <sub_repo>..."
#cd ${PROJECT_DIR}/<sub_repo>
#<make_command>
#PSTAT=$?
#if [ "$PSTAT" != "0" ]; then
#  echo "<sub_repo> make failed!"
#  read -p "Press [Enter] key to finish..."
#  exit 1
#fi

# pushd .  ;  source ~/.bashrc  ;  popd 

printf "\n\ncd $PROJECT_DIR\n\n" >> ~/.bashrc
sleep 1
pushd .
sleep 1
source $HOME/.bashrc
sleep 1
popd
sleep 1


# installDependencies
cd ${PROJECT_DIR}/startup
./installDependencies.sh ${PROJECT_DIR}
PSTAT=$?
if [ "$PSTAT" != "0" ]; then
  # could not install all dependencies
  exit 1
fi
sleep 1


# build ROS stuff
cd ${PROJECT_DIR}/startup
./buildRos.sh ${PROJECT_DIR}
PSTAT=$?
if [ "$PSTAT" != "0" ]; then
  # error message written in 'buildRos.sh'
  exit 1
fi

# build pax
echo
echo "***************"
echo "Building PAX..."
cd ${PROJECT_DIR}/pride/automate/code
ant clean
ant
PSTAT=$?
if [ "$PSTAT" != "0" ]; then
  echo "PAX make failed!"
  read -p "Press [Enter] key to finish..."
  exit 1
fi

# build prideview
echo
echo "*********************"
echo "Building PrideView..."
cd ${PROJECT_DIR}/pride/view/code


npm run init
npm run mysql-reset


PSTAT=$?
if [ "$PSTAT" != "0" ]; then
  echo "PrideView make failed!"
  read -p "Press [Enter] key to finish..."
  exit 1
fi

if [ "${1}" != "--no-pause" ]; then
  read -p "Press [Enter] key to finish..."
fi
#echo
#echo "****************************"
#echo "Intentionally exiting script"
#echo "****************************"
#exit 0

