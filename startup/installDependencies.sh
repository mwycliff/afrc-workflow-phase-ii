#!/bin/bash
# Install dependencies for afrc-workflow-phase-II


sudo apt install xdotool
PSTAT=$?
if [ "$PSTAT" != "0" ]; then
  # error: could not install xdotool for some reason
  exit 1
fi

sudo apt install default-jdk
PSTAT=$?
if [ "$PSTAT" != "0" ]; then
  # error: could not install ant for some reason
  exit 1
fi

sudo apt install ant
PSTAT=$?
if [ "$PSTAT" != "0" ]; then
  # error: could not install npm for some reason
  exit 1
fi

#sudo apt install npm
#PSTAT=$?
#if [ "$PSTAT" != "0" ]; then
#  # error: could not install npm for some reason
#  exit 1
#fi

sleep 1
echo "* * *                  * * *"
sleep 1
echo "DONE INSTALLING DEPENDENCIES"
sleep 1
echo "* * *                  * * *"
echo "\n\n\n\n"



