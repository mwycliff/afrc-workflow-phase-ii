#!/bin/bash
# specific project environment setup
# use $PROJECT_DIR in calling scripts for project's root directory
#
# parameters:
# --no-bashrc-mods
#
# exit codes (check using $?):
# 0: success
# 1: failure
#
# check $BASHRC_MOD for .bashrc contents:
# 0: already exists
# 1: modified, re-source
# 2: non-existent, unmodified

# set project name, dictates software environment vars
#export PROJECT='AFRC-WORKFLOW-PHASE-II'
export PROJECT='AFRC_WORKFLOW_PHASE_II'

# shouldn't have to change anything else...
SKIP_MOD=0
if [ "$1" = "--no-bashrc-mods" ]; then
  SKIP_MOD=1
fi
PROJECT_NAME_VAR="${PROJECT}_DIR"
BASHRC_MOD=0
VAR_CHECK=`export | grep ${PROJECT_NAME_VAR}`
if [ "$VAR_CHECK" = "" ]; then
  echo
  echo "*** Missing exported $PROJECT PROJECT_DIR environment variable"
  if [ "$SKIP_MOD" != "1" ]; then
    BASHRC_CHECK=`cat ~/.bashrc | grep $PROJECT_NAME_VAR`
	 if [ "$BASHRC_CHECK" = "" ]; then
      echo "*** The $PROJECT PROJECT_DIR environment variable is"
      echo "*** required for various installation and execution scripts."
  	   read -e -r -p "*** Write to .bashrc (yes/no)? "
      if [ "$REPLY" = "yes" ] || [ "$REPLY" = "YES" ]; then
        PROJECT_DIR=`pwd`
        export $PROJECT_NAME_VAR=$PROJECT_DIR
        echo "*** Adding $PROJECT PROJECT_DIR environment variable to .bashrc..."
        echo "" >> ~/.bashrc
        echo "# auto-written env var for $PROJECT PROJECT_DIR" >> ~/.bashrc
        echo "export $PROJECT_NAME_VAR=$PROJECT_DIR" >> ~/.bashrc
        BASHRC_MOD=2
      else
        echo "*** Not modifying .bashrc..."
        BASHRC_MOD=1
      fi
    else
      BASHRC_MOD=2
    fi
  fi
  # refresh VAR_CHECK, check again
  VAR_CHECK=`export | grep ${PROJECT_NAME_VAR}`
else
  # environment root set, put in PROJECT_DIR environment variable
  PROJECT_DIR=${!PROJECT_NAME_VAR}
fi

if [ "$BASHRC_MOD" = "2" ]; then
  # only happens when NOT skipping bashrc mod, so prompt is OK
  echo "*** But found $PROJECT PROJECT_DIR environment variable!"
  return 0
elif [ "$VAR_CHECK" = "" ]; then
  # re-check environment, return code error=1
  return 1
fi
return 0

